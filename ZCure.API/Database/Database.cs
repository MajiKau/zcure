﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models;
using ZCure.API.Models.Clan;
using ZCure.API.Models.Store;

namespace ZCure.API.Database
{
    public class Repositories
    {
        public Repository<User, long> Users { get; private set; }
        public Repository<StoreItem, Guid> Store { get; private set; }
        public Repository<InventoryItem, Guid> Inventory { get; private set; }
        public Repository<UnlockLevel, int> Levels { get; private set; }
        public Repository<StoreOfferItem, Guid> Offers { get; private set; }
        public Repository<UserStats, long> Stats { get; private set; }
        public Repository<Loadout, Guid> Loadouts { get; private set; }
        public Repository<ZCFriendItem, long> Friends { get; private set; }
        public Repository<ZCClan, Guid> Clans { get; private set; }
        public Repository<ZCClanMember, long> ClanMembers { get; set; }

        private ZCureApp _app;

        public Repositories(ZCureApp app)
        {
            _app = app;
            Users = new Repository<User, long>(app);
            Store = new Repository<StoreItem, Guid>(app);
            Inventory = new Repository<InventoryItem, Guid>(app);
            Levels = new Repository<UnlockLevel, int>(app);
            Offers = new Repository<StoreOfferItem, Guid>(app);
            Stats = new Repository<UserStats, long>(app);
            Loadouts = new Repository<Loadout, Guid>(app);
            Friends = new Repository<ZCFriendItem, long>(app);
            Clans = new Repository<ZCClan, Guid>(app);
            ClanMembers = new Repository<ZCClanMember, long>(app);
        }
    }
}
