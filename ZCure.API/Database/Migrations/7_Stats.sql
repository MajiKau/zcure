CREATE TABLE IF NOT EXISTS `Stats` (
  `UserId` bigint NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `ModifiedAt` datetime NOT NULL,
  `Data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;