﻿CREATE TABLE IF NOT EXISTS `Clans` (
  `Id` varchar(36) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Tag` varchar(6) NOT NULL,
  `OwnerId` bigint NOT NULL,
  `MOTD` text NOT NULL,
  `FoundedAt` datetime NOT NULL,
  `MemberLimit` smallint NOT NULL,
  PRIMARY KEY(`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `ClanMembers` (
	`ClanId` varchar(36) NOT NULL,
	`MemberId` bigint NOT NULL,
	`InviterId` bigint NOT NULL,
	`JoinedAt` datetime NOT NULL,
	`Rank` tinyint(2) NOT NULL,
	PRIMARY KEY(`MemberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;