﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZCure.API.Managers
{
    public class ZCFileManager
    {
        protected Dictionary<string, byte[]> FileCache = new Dictionary<string, byte[]>();

        private ILogger log;

        public ZCFileManager()
        {
            log = Log.ForContext<ZCFileManager>();
        }

        public byte[]? GetFile(string filename)
        {
            if (!FileCache.ContainsKey(filename))
            {
                log.Verbose("Failed to get content for {File}: not found", filename);
                return null;
            }

            return FileCache[filename];
        }

        public void SetFile(string filename, byte[] content)
        {
            FileCache[filename] = content;
        }
    }
}
