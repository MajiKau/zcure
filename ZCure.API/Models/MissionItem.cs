using System.Xml.Serialization;

namespace ZCure.API.Models
{
    public class MissionItem
    {
        [XmlAttribute("MK")]
        public string Name {get; set;}

        [XmlAttribute("MV")]
        public float Value {get; set;}

        [XmlAttribute("ID")]
        public DateTime InitiatedAt {get; set;}

        [XmlAttribute("RD")]
        public DateTime ResetAt {get; set;}

        [XmlAttribute("NC")]
        public int Completions {get; set;}

        [XmlAttribute("CS")]
        public EMissionCompletionState CompletionState {get; set;}
    }
}