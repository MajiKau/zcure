using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    
    [XmlRoot("MBL")]
    public class ZCClanMemberCombinedInfo
    {
        [XmlAttribute("UID")]
        public long MemberId { get; set; }

        [XmlAttribute("UN")]
        public string Name { get; set; }

        [XmlAttribute("LS")]
        public DateTime LastSeenAt { get; set; }

        [XmlAttribute("CP")]
        public EClanRank Rank { get; set; }

        [XmlAttribute("BD")]
        public long Badge { get; set; }

        [XmlAttribute("TD")]
        public long Title { get; set; }

        [XmlAttribute("XP")]
        public int Experience { get; set; }
    }
}