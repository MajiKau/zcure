using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    [XmlRoot("CCI")]
    public class ZCClanCombinedInfo
    {
        [XmlElement("CI")]
        public ZCClan Clan {get; set;}

        [XmlElement("ML")]
        public List<ZCClanMemberCombinedInfo> Members {get; set;} = new List<ZCClanMemberCombinedInfo>{};
        
        [XmlElement("IV")]
        public List<ZCClanInviteInfo> Invites { get; set; } = new List<ZCClanInviteInfo>{};
    }
}