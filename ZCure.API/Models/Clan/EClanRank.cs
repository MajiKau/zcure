using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    [Serializable]
    public enum EClanRank
    {
        [XmlEnum("0")]
        Leader,
        [XmlEnum("1")]
        Officer,
        [XmlEnum("2")]
        Member,
        [XmlEnum("3")]
        Recruit,
        [XmlEnum("4")]
        Invited
    }
}