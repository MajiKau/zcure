using System.Xml.Serialization;

namespace ZCure.API.Models.Clan
{
    public class ZCClanInfo
    {
        [XmlElement("CI")]
        public ZCClan Clan { get; set; }

        [XmlArray("ML")]
        public List<ZCClanMember> Members { get; set; }

        public ZCClanInfo(ZCClan clan, List<ZCClanMember> members)
        {
            Clan = clan;
            Members = members;
        }
    }
}