﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [Table("Loadouts")]
    public class Loadout
    {
        [XmlAttribute("ID")]
        public Guid Id { get; set; } = Guid.NewGuid();
        
        [XmlAttribute("ParentID")]
        public Guid ParentId { get; set; } = Guid.Empty;

        [XmlAttribute("Name")]
        public string Name { get; set; } = "New Loadout";

        [XmlAttribute("Owner")]
        public long OwnerId { get; set; }

        [XmlAttribute("Creator")]
        public long CreatorId { get; set; }

        [NotMapped]
        [XmlAttribute("CreatorName")]
        public string CreatorName = "";

        [XmlAttribute("CreateTime")]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [XmlAttribute("LastModified")]
        public DateTime ModifiedAt { get; set; } = DateTime.Now;
        
        [XmlAttribute("Data")]
        public string Data { get; set; } = "";

        [XmlAttribute("Installed")]
        public int Installed { get; set; } = 0;

        public Guid ShareCode { get; set; } = Guid.Empty;

        [XmlAttribute("ShareCode")]
        [NotMapped]
        public string ShareCodeString
        {
            get { return ShareCode == Guid.Empty ? "" : ShareCode.ToString(); }
            set { ShareCode = Guid.Parse(value); }
        }

        public Loadout Clone()
        {
            return new Loadout()
            {
                Id = Guid.NewGuid(),
                OwnerId = OwnerId,
                CreatorId = CreatorId,
                CreatedAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                Name = Name,
                Data = Data
            };
        }
    }
}
