﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ZCure.API.Serializers;

namespace ZCure.API.Models
{
    public class UserStat
    {
        [XmlAttribute("N")]
        public string Name;

        [XmlAttribute("V")]
        public float Value;

        [XmlAttribute("S")]
        public string StringValue;
    }

    [Table("Stats")]
    [XmlRoot("StatsItem")]
    public class UserStats
    {
        [Key]
        [XmlAttribute("ID")]
        public long UserId { get; set; }

        [XmlIgnore]
        public User User { get; set; }

        [XmlAttribute("PN")]
        [NotMapped]
        public string PlayerName = ""; //{ get {return User.Name;} set {} }

        [XmlAttribute("CT")]
        public DateTime CreatedAt { get; set; }

        [XmlAttribute("WT")]
        public DateTime ModifiedAt { get; set; }

        [XmlAttribute("DT")]
        public string Data { get; set; }

        [XmlIgnore]
        [NotMapped]
        public List<UserStat> Stats
        {
            get
            {
                if (string.IsNullOrEmpty(Data))
                    return new List<UserStat>();
                var stats = ZCXmlSerializer.Deserialize<List<UserStat>>(Data);
                stats.RemoveAll(s => string.IsNullOrEmpty(s.Name));
                return stats;
            }

            set
            {
                value.Sort();
                Data = ZCXmlSerializer.Serialize(value);
            }
        }
    }
}
