using System.ComponentModel;
using System.Xml.Serialization;

namespace ZCure.API.Models
{
    [XmlRoot("PU")]
    public class PermanentItem
    {
        [XmlAttribute("ID")]
        public int UnlockId { get; set; }

        [XmlAttribute("UID")]
        public Guid InstanceId { get; set; }

        [XmlAttribute("AT")]
        [DefaultValue(-1)]
        public int AssociatedUnlockId { get; set; } = -1;

        [XmlAttribute("SN")]
        [DefaultValue(true)]
        public bool Seen { get; set; } = true;
    }
}