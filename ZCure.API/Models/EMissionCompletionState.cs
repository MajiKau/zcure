namespace ZCure.API.Models
{
    public enum EMissionCompletionState
    {
        EMCS_NotStarted,
        EMCS_Started,
        EMCS_Complete,
        EMCS_Redeemed,
    }
}