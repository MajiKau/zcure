﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models.Store
{
    public enum ECurrencyType
    {
        ECT_Game,
        ECT_Premium,
    }

    [XmlRoot("SIP")]
    public class StoreItemPrice
    {
        [XmlAttribute("CUR")]
        public ECurrencyType Currency { get; set; }

        [XmlAttribute("DUR")]
        public int DurationHours { get; set; }

        [XmlAttribute("PR")]
        public int Price { get; set; }

        [XmlAttribute("DC")]
        [DefaultValue(1f)]
        public float Discount { get; set; }

        [XmlAttribute("CNT")]
        [DefaultValue(1)]
        public int Count { get; set; }
    }
}
