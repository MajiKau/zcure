﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models.Store
{
    [XmlRoot("SIMD")]
    public class StoreItemMetaData
    {
        [XmlAttribute("FN")]
        public string FriendlyName;

        [XmlAttribute("IT")]
        public string ItemTags;

        [XmlAttribute("RA")]
        public int Rarity;
    }
}
