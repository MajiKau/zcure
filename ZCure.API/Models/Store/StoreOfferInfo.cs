﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZCure.API.Models.Store
{
    [XmlRoot("ZCStoreOfferInfo")]
    public class StoreOfferInfo
    {
        [XmlElement("SO")]
        public List<StoreOfferItem> OfferItems { get; set; } = new List<StoreOfferItem>();
    }
}
