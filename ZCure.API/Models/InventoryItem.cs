﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ZCure.API.Models.Store;

namespace ZCure.API.Models
{
    public enum EPurchaseType
    {
        Inventory,
        Activate,
        Mail,
        Gift
    }

    [XmlRoot("ZCInventoryItem")]
    [Table("Inventory")]
    public class InventoryItem : ICloneable
    {
        [XmlAttribute("ID")]
        public int UnlockId { get; set; }
        
        [XmlAttribute("AT")]
        [DefaultValue(-1)]
        public int AssociatedUnlockId { get; set; }

        [Key]
        [XmlAttribute("GID")]
        public Guid InstanceId { get; set; }

        [XmlAttribute("SID")]
        public Guid StoreItemId { get; set; }

        [XmlIgnore]
        public User Owner  { get; set; }

        [XmlAttribute("OID")]
        [NotMapped]
        public string OwnerIdSer => Owner.Id.ToString("D19");

        [XmlAttribute("USE")]
        [DefaultValue(-1)]
        public int Quantity { get; set; }

        [XmlAttribute("AC")]
        public bool Activated { get; set; }

        [XmlAttribute("XD")]
        public DateTime ExpireAt { get; set; }

        [XmlAttribute("PTD")]
        public int PurchaseDuration { get; set; }

        [XmlAttribute("PT")]
        [DefaultValue(EPurchaseType.Activate)]
        public EPurchaseType PurchaseType { get; set; }

        [XmlAttribute("SN")]
        [DefaultValue(true)]
        public bool Seen { get; set; }

        [XmlAttribute("MD")]
        [DefaultValue("")]
        public string MetaData { get; set; } = "";

        [XmlAttribute("REM")]
        [DefaultValue(false)]
        public bool Removed { get; set; }

        [XmlAttribute("UTK")]
        [DefaultValue("")]
        public string UpdateTypeKey { get; set; } = "";

        [XmlIgnore]
        [NotMapped]
        public bool IsPermanent => PurchaseDuration == -1;

        public PermanentItem ToPermanentItem() => new PermanentItem()
        {
            UnlockId = UnlockId,
            InstanceId = InstanceId,
            AssociatedUnlockId = AssociatedUnlockId,
            Seen = Seen
        };

        public static InventoryItem FromStoreItem(StoreItem storeItem, User owner)
        {
            return new InventoryItem()
            {
                Owner = owner,
                UnlockId = storeItem.ItemId,
                InstanceId = Guid.NewGuid(),
                StoreItemId = storeItem.Id,
                Quantity = -1,
                Activated = true,
                ExpireAt = DateTime.Now.AddMonths(1),
                PurchaseDuration = 10,
                PurchaseType = EPurchaseType.Inventory,
                Seen = true,
                Removed = false
            };
        }

        public object Clone()
        {
            return new InventoryItem()
            {
                Owner = Owner,
                UnlockId = UnlockId,
                InstanceId = Guid.NewGuid(),
                StoreItemId = StoreItemId,
                Quantity = Quantity,
                Activated = Activated,
                ExpireAt = ExpireAt,
                PurchaseDuration = PurchaseDuration,
                PurchaseType = PurchaseType,
                Seen = Seen,
                Removed = Removed
            };
        }
    }
}
