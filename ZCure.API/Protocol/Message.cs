﻿using zlib;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Crypt;
using System.IO.Compression;

namespace ZCure.API.Protocol
{
    public class Message
    {
        public static byte[] PrefixBytes = new byte[] { 60, 60 };
        public static byte[] PostfixBytes = new byte[] { 62, 62 };
        public static byte[] CompressFlagBytes = new byte[] { 90, 88 };

        public MessageHeader Header { get; private set; }

        public MessageType MessageType => Header.MessageType;

        public ushort SequenceNum
        {
            get { return Header.SequenceNum; }
            set { Header.SequenceNum = value; }
        }

        public ushort Padding => Header.Padding;

        public byte[] Data { get; set; }

        protected static bool HasValidPrefix(byte[] data) =>
            data.Take(PrefixBytes.Length).SequenceEqual(PrefixBytes);

        protected static bool HasValidPostfix(byte[] data) =>
            data.TakeLast(PostfixBytes.Length).SequenceEqual(PostfixBytes);

        protected static bool IsBodyCompressed(byte[] data) =>
            data.Take(CompressFlagBytes.Length).SequenceEqual(CompressFlagBytes);

        public Message(MessageHeader header, byte[] data)
        {
            Header = header;
            Data = data;
        }

        public Message(MessageType messageType, ushort sequenceNum, byte[] data)
        {
            Header = new MessageHeader(messageType, sequenceNum, 0);
            Data = data;
        }

        public static Message Decrypt(byte[] encryptedData)
        {
            if (!HasValidPrefix(encryptedData))
                throw new Exception($"Encrypted message has invalid prefix ({encryptedData.Take(2)})");

            if (!HasValidPostfix(encryptedData))
                throw new Exception($"Encrypted message has invalid postfix ({encryptedData.TakeLast(2)})");

            int messageLength = encryptedData.Length - 4;
            var decryptedData = new byte[messageLength];

            Array.Copy(encryptedData, 2, decryptedData, 0, messageLength);
            XXTEA.DoDecrypt(ref decryptedData);

            var header = new MessageHeader(decryptedData.Take(8).ToArray());
            if (!header.IsValid)
                throw new Exception($"Message has invalid header");

            int encryptedBodyLength = messageLength - 8;
            var decryptedBody = new byte[encryptedBodyLength];

            Array.Copy(decryptedData, 8, decryptedBody, 0, encryptedBodyLength);
            XXTEA.DoDecrypt(ref decryptedBody);

            var body = decryptedBody.Take(encryptedBodyLength - header.Padding).ToArray();

            if (IsBodyCompressed(body))
                body = Decompress(body);

            return new Message(header, body);
        }

        public byte[] Encrypt(bool compressData = false)
        {
            byte[] encryptedBody = Data ?? new byte[] { 1 };

            if (compressData)
                encryptedBody = Compress(encryptedBody);

            Header.Padding = XXTEA.DoEncrypt(ref encryptedBody);

            var messageHeader = Header.Serialize();
            var encryptedData = new byte[encryptedBody.Length + messageHeader.Length];
            messageHeader.CopyTo(encryptedData, 0);
            encryptedBody.CopyTo(encryptedData, messageHeader.Length);

            XXTEA.DoEncrypt(ref encryptedData);
            var encryptedMessage = new byte[encryptedData.Length + 4];

            PrefixBytes.CopyTo(encryptedMessage, 0);
            encryptedData.CopyTo(encryptedMessage, PrefixBytes.Length);
            PostfixBytes.CopyTo(encryptedMessage, PrefixBytes.Length + encryptedData.Length);

            return encryptedMessage;
        }

        public static byte[] Decompress(byte[] data)
        {
            int len = BitConverter.ToInt32(data, 2);
            var buffer = new byte[len];

            using(var mem = new MemoryStream(buffer))
            using(var zout = new ZOutputStream(mem))
            {
                zout.Write(data, 6, data.Length - 6);
            }

            return buffer;
        }

        public static byte[] Compress(byte[] data)
        {
            byte[] compressedData = [];

            using(var mem = new MemoryStream())
            using(var zin = new ZLibStream(mem, CompressionLevel.Fastest))
            {
                zin.Write(data, 0, data.Length);
                compressedData = mem.ToArray();
            }
            
            byte[] compressed = new byte[2]{90, 88}
                .Concat(BitConverter.GetBytes(data.Length))
                .Concat(compressedData)
                .ToArray();

            return compressed;
        }
    }
}
