﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Connections;
using ZCure.API.Database;
using ZCure.API.Servers;

namespace ZCure.API.Requests
{
    public abstract class Controller
    {
        protected ILogger _log;

        public ISession Session { get; set; }

        protected ZCureApp _app => Session.App;
        protected Repositories _database => _app.Database;

        public Controller()
        {
            _log = Log.ForContext(GetType());
        }
    }
}
