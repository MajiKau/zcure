﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models;

namespace ZCure.API.Caches
{
    public class AuthTicket
    {
        public static TimeSpan Lifespan = TimeSpan.FromHours(10);
        public long UserId { get; set; } = 0;
        public DateTime Created { get; private set; } = DateTime.Now;
        public IPAddress IP { get; set; } = IPAddress.None;

        public string AuthKey { get; private set; }

        public bool IsValid => Created.Add(Lifespan) > DateTime.Now;

        public AuthTicket(long userId, IPAddress ip)
        {
            UserId = userId;
            IP = ip;
            AuthKey = Encoding.UTF8.GetString(SHA256.HashData(Encoding.UTF8.GetBytes($"{userId}:{ip}:{DateTime.Now}")));
        }
    }

    public class AuthCache : SynchronizedCache<long, AuthTicket>
    {
        public override TimeSpan UpdateInterval => TimeSpan.FromMinutes(1);

        public override void Sync()
        {
            foreach(var ticket in _itemCache.Values)
            {
                if (!ticket.IsValid)
                {
                    _log.Debug("removing outdated auth ticket for {UserId} ({IP})", ticket.UserId, ticket.IP);
                    _itemCache.Remove(ticket.UserId);
                }
            }
        }

        public AuthTicket? GetByAddress(IPAddress ip)
        {
            lock(_itemCache)
            {
                return _itemCache.Values.Where(ticket => ticket.IP.Equals(ip)).FirstOrDefault();
            }
        }
    }
}
