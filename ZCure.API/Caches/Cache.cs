﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace ZCure.API.Caches
{
    public class Cache<TKey, TItem> : ICache<TKey, TItem> where TKey : notnull
    {
        protected Dictionary<TKey, TItem> _itemCache;

        protected ILogger _log;

        public Cache()
        {
            _itemCache = new Dictionary<TKey, TItem>();
            _log = Log.ForContext(GetType());
        }

        public virtual void Add(TKey key, TItem item)
        {
            lock(_itemCache)
            {
                _itemCache.Add(key, item);
            }
        }

        public virtual bool Contains(TKey key)
        {
            lock(_itemCache)
            {
                return _itemCache.ContainsKey(key);
            }
        }

        public virtual bool Contains(Predicate<TItem> predicate)
        {
            lock(_itemCache)
            {
                return _itemCache.Where(pair => predicate(pair.Value)).Count() > 0;
            }
        }

        public virtual TItem? Get(TKey key)
        {
            lock(_itemCache)
            {
                return _itemCache.GetValueOrDefault(key);
            }
        }

        public virtual void Update(TKey key, TItem item)
        {
            lock(_itemCache)
            {
                _itemCache[key] = item;
            }
        }

        public virtual void Remove(TKey key)
        {
            lock(_itemCache)
            {
                _itemCache.Remove(key);
            }
        }

        public virtual IEnumerable<TItem> GetAll()
        {
            lock(_itemCache)
            {
                return _itemCache.Values;
            }
        }

        public virtual IEnumerable<TItem> Where(Predicate<TItem> predicate)
        {
            lock(_itemCache)
            {
                return _itemCache.Where(pair => predicate(pair.Value)).Select(pair => pair.Value);
            }
        }

        public virtual IEnumerable<TItem> Where(Func<TKey, TItem, bool> predicate)
        {
            lock(_itemCache)
            {
                return _itemCache.Where(pair => predicate(pair.Key, pair.Value)).Select(pair => pair.Value);
            }
        }
    }
}
