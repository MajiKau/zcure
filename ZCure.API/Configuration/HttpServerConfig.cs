namespace ZCure.API.Configuration
{
    public class HttpServerConfig : IConfig
    {
        public int Port { get; set; } = 80;
        public string Ip { get; set;} = "0.0.0.0";

        public bool ReverseProxy { get; set; } = false;
        public string ProxyRefererHeader { get; set; } = "X-Forwarded-For";
    }
}