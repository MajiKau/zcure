﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ZCure.API.Serializers
{
    public class ZCXmlSerializer
    {
        protected static Dictionary<Type, XmlSerializer> SerializerCache = new Dictionary<Type, XmlSerializer>();

        public static XmlSerializer GetSerializer(Type xmlType)
        {
            XmlSerializer serializer;
            if (!SerializerCache.TryGetValue(xmlType, out serializer))
            {
                serializer = new XmlSerializer(xmlType);
                SerializerCache.Add(xmlType, serializer);
            }

            return serializer;
        }

        public static XmlSerializer GetSerializer<T>()
        {
            return GetSerializer(typeof(T));
        }

        public static string Serialize(Type xmlType, object obj)
        {
            var serializer = GetSerializer(xmlType);

            using(var memoryStream = new MemoryStream())
            using(var writer = new StreamWriter(memoryStream))
            {
                serializer.Serialize(writer, obj);
                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }

        public static string Serialize<T>(T obj)
        {
            return Serialize(typeof(T), obj);
        }

        public static object Deserialize(Type xmlType, string data)
        {
            var serializer = GetSerializer(xmlType);

            var reader = new StringReader(data);
            return serializer.Deserialize(reader);

        }

        public static T Deserialize<T>(string data)
        {
            return (T)Deserialize(typeof(T), data);
        }
    }
}
