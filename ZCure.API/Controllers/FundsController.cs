﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models;
using ZCure.API.Models.Store;
using ZCure.API.Protocol;
using ZCure.API.Requests;

namespace ZCure.API.Controllers
{
    public class FundsController : Controller
    {
        [Query(MessageType.GetFunds)]
        public (bool, long, int, int) GetFunds(User? user)
        {
            if (user == null)
                return (false, 0, 0, 0);

            return (true, user.Id, user.GP, user.ZP);
        }

        [Query(MessageType.GetGP)]
        public (bool, long, int) GetGP(User? user)
        {
            if (user == null)
                return (false, 0, 0);

            return (true, user.Id, user.GP);
        }

        [Query(MessageType.AddGP)]
        public (bool, long, int) AddGP(User? user, int amount)
        {
            if (user == null)
                return (false, 0, 0);

            user.GP = AddBalance(user, ECurrencyType.ECT_Game, user.GP, amount);
            _database.Users.Update(user);
            return (true, user.Id, user.GP);
        }

        [Query(MessageType.RemoveGP)]
        public (bool, long, int) RemoveGP(User? user, int amount)
        {
            if (user == null)
                return (false, 0, 0);

            user.GP = RemoveBalance(user, ECurrencyType.ECT_Game, user.GP, amount);
            _database.Users.Update(user);
            return (true, user.Id, user.GP);
        }

        [Query(MessageType.GetZP)]
        public (bool, long, int) GetZP(User? user)
        {
            if (user == null)
                return (false, 0, 0);

            return (true, user.Id, user.ZP);
        }

        [Query(MessageType.AddZP)]
        public (bool, long, int) AddZP(User? user, int amount)
        {
            if (user == null)
                return (false, 0, 0);

            user.ZP = AddBalance(user, ECurrencyType.ECT_Premium, user.ZP, amount);
            _database.Users.Update(user);
            return (true, user.Id, user.ZP);
        }

        [Query(MessageType.RemoveZP)]
        public (bool, long, int) RemoveZP(User? user, int amount)
        {
            if (user == null)
                return (false, 0, 0);

            user.ZP = RemoveBalance(user, ECurrencyType.ECT_Premium, user.ZP, amount);
            _database.Users.Update(user);
            return (true, user.Id, user.ZP);
        }


        protected int AddBalance(User user, ECurrencyType currency, int balance, int amount)
        {
            int newBalance = balance + amount;
            _log.Debug("added {Amount} {Currency} to {User}, new balance: {Balance}", amount, CurrencyToString(currency), user, newBalance);
            return newBalance;
        }

        protected int RemoveBalance(User user, ECurrencyType currency, int balance, int amount)
        {
            if(balance - amount < 0)
            {
                _log.Debug("removing {Amount} {Currency} from {User} failed: balance not sufficient ({Balance})", amount, CurrencyToString(currency), user, balance);
                return balance;
            }

            int newBalance = balance - amount;
            _log.Debug("removed {Amount} {Currency} from {User}, new balance: {Balance}", amount, CurrencyToString(currency), user, newBalance);
            return newBalance;
        }

        protected string CurrencyToString(ECurrencyType currency)
        {
            switch(currency)
            {
                case ECurrencyType.ECT_Game: return "GP";
                case ECurrencyType.ECT_Premium: return "ZP";
                default: return "";
            }
        }
    }
}
