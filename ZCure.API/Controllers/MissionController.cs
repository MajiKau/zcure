using ZCure.API.Protocol;
using ZCure.API.Models;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class MissionController : Controller
    {
        [Query(MessageType.GetMissions)]
        public (bool, string, string) GetMissions(long userId)
        {
            var missions = new ZCMission() { Items = new List<MissionItem>(), OwnerId = userId };
            var data = ZCXmlSerializer.Serialize(missions);
            return (true, userId.ToString("D19"), data);
        }
    }
}