﻿using ZCure.API.Requests;
using ZCure.API.Protocol;
using System.Text;
using ZCure.API.Models;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public enum EPlayerNameValidationState
    {
        Valid,
        InvalidChars,
        InUse,
        Unknown
    }

    public class UserController : Controller
    {
        [Query(MessageType.GetProfile)]
        public (bool, long, string, int, bool, int, int, int, DateTime, int) GetProfile(User? user)
        {
            if (user == null)
                return (false, 0, "", 0, false, 0, 0, 0, DateTime.MinValue, 0);

            return (true, user.Id, user.Name, user.Experience, user.Gender == EGender.Female,
                (int)user.ProfileFlags, (int)user.Badge, (int)user.Title, user.LastSeenAt, user.PrestigeLevel);
        }

        [Query(MessageType.GetProfileBatch)]
        public dynamic GetProfileBatch(string userIdStr)
        {
            List<long> userIds = userIdStr.Split(":").Select(long.Parse).ToList();

            var userProfiles = userIds.Select(_database.Users.Get).Where(u => u != null)
                .Select(u => u!.ResponseString);

            return (true, String.Join(":", userProfiles));
        }

        [Query(MessageType.UpdateProfile)]
        public bool UpdateProfile(User? user, int title, int badge)
        {
            if (user == null)
                return false;

            user.Title = (EProfileTitle)title;
            user.Badge = (EProfileBadge)badge;
            _database.Users.Update(user);

            _log.Debug("updated profile of {User}: Title = {Title}, Badge = {Badge}", user, title, badge);
            return true;
        }

        [Query(MessageType.GetUserSettings)]
        public dynamic GetUserSettings(User? user)
        {
            if (user == null)
                return false;

            return (true, user.Settings ?? []);
        }

        [Query(MessageType.UpdateUserSettings)]
        public bool UpdateUserSettings(User? user, byte[] settings)
        {
            if (user == null)
                return true;

            user.Settings = settings;
            _database.Users.Update(user);
            _log.Debug("updated user settings of {User}", user);
            return true;
        }

        [Query(MessageType.ValidatePlayerName)]
        public (bool, long, int) ValidatePlayerName(long userId, string name)
        {
            var namedUser = _database.Users.Where(new { Name = name }).FirstOrDefault();

            if (namedUser != null && namedUser.Id != userId)
                return (false, userId, (int)EPlayerNameValidationState.InUse);

            return (true, userId, (int)EPlayerNameValidationState.Valid);
        }

        [Query(MessageType.CreateCharacter)]
        public dynamic CreateCharacter(User? user, string name, string flags)
        {
            if (user == null)
                return false;

            var profileFlags = flags.Split(",")
                .Select(s => (EProfileFlag)int.Parse(s))
                .Aggregate((a, b) => a | b);

            user.Name = name;
            user.Experience = 0;
            user.ProfileFlags |= profileFlags | EProfileFlag.AccountMigrated | EProfileFlag.TrainingComplete 
                | EProfileFlag.FirstPurchase1 | EProfileFlag.FirstPurchase2 | EProfileFlag.FirstPurchase3 
                | EProfileFlag.FirstPurchase4 | EProfileFlag.FirstPurchase5 | EProfileFlag.FirstPurchase6;

            _database.Users.Update(user);
            _log.Debug("created character for {User}", user);
            return (true, user.Name);
        }
    }
}

