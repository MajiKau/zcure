﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Models;
using ZCure.API.Requests;
using ZCure.API.Servers;

namespace ZCure.API.Controllers
{
    public class PresenceController : Controller
    {
        [Query(Protocol.MessageType.UpdatePresence, Server: EServerType.Presence)]
        public void UpdatePresence(long userId, User? user, string data)
        {
            _log.Debug($"update presence: {data}");
        }
    }
}
