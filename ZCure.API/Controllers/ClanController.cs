using ZCure.API.Protocol;
using ZCure.API.Models.Clan;
using ZCure.API.Requests;
using ZCure.API.Serializers;
using ZCure.API.Models;

namespace ZCure.API.Controllers
{
    public class ClanController : Controller
    {
        [Query(MessageType.GetClanInfo)]
        public (bool, string) GetClanInfo(string clanTag)
        {
            ZCClan? clan = null;
            if(clanTag == "Invalid Clan Tag")
            {
                var clanMember = _database.ClanMembers.Get(Session.UserId);
                if(clanMember != null)
                    clan = _database.Clans.Get(clanMember.ClanId);
            }
            else
            {
                clan = _database.Clans.Where(new { Tag = clanTag }).FirstOrDefault();
            }

            
            if (clan == null)
                return (false, "");

            // @todo: fix client crash with clan members combined member info
            /*var clanMembers = _database.ClanMembers.Where(new { ClanId = clan.Id });

            var clanMemberCombined = clanMembers.Select(cm => new KeyValuePair<ZCClanMember, User>(cm, _database.Users.Get(cm.MemberId)))
                .Select(cmu => new ZCClanMemberCombinedInfo()
                {
                    MemberId = cmu.Key.MemberId,
                    Rank = cmu.Key.Rank,
                    Name = cmu.Value.Name,
                    LastSeenAt = cmu.Value.LastSeenAt,
                    Badge = (long)cmu.Value.Badge,
                    Title = (long)cmu.Value.Title,
                    Experience = cmu.Value.Experience
                }).ToList();*/



            var clanInfo = ZCXmlSerializer.Serialize(new ZCClanCombinedInfo() { Clan = clan, /*Members = clanMemberCombined */});
            return (true, clanInfo);
        }

        [Query(MessageType.GetClanTag)]
        public dynamic GetClanTag(long userId)
        {
            var clanMember = _database.ClanMembers.Get(userId);
            
            if(clanMember != null)
            {
                var clan = _database.Clans.Get(clanMember.ClanId);
                if(clan != null)
                {
                    return (true, userId, clan.Tag);
                }
            }

            return (false, $"{userId}:");
        }

        [Query(MessageType.ValidateClanInfo)]
        public dynamic ValidateClanInfo(int type, string data)
        {
            switch(type)
            {
                case 1:
                    return (true, ValidateClanName(data) ? 'T' : 'F', type);
                case 0:
                    return (true, ValidateClanTag(data)? 'T' : 'F', type);
            }

            return false;
        }

        [Query(MessageType.CreateClan)]
        public dynamic CreateClan(User creator, string data)
        {
            var info = data.Split(";");
            
            var clan = new ZCClan()
            {
                Id = Guid.NewGuid(),
                Name = info[0],
                Tag = info[1],
                MOTD = info[2] + " " + info[0],
                FoundedAt = DateTime.Now,
                OwnerId = creator.Id,
                MemberLimit = 1000
            };

            _database.Clans.Insert(clan);

            var clanMember = new ZCClanMember()
            {
                MemberId = creator.Id,
                InviterId = creator.Id,
                JoinedAt = DateTime.Now,
                ClanId = clan.Id,
                Rank = EClanRank.Leader
            };
            _database.ClanMembers.Insert(clanMember);

            return (1, 1);
        }

        public bool ValidateClanName(string name)
        {
            return _database.Clans.Where(new { Name = name }).Count() == 0;
        }

        public bool ValidateClanTag(string tag)
        {
            return _database.Clans.Where(new { Tag = tag }).Count() == 0;
        }
    }
}