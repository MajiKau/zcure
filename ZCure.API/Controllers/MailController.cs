using ZCure.API.Protocol;
using ZCure.API.Requests;
using ZCure.API.Serializers;

namespace ZCure.API.Controllers
{
    public class MailController : Controller
    {
        [Query(MessageType.GetMails)]
        public (bool, string) QueryUserMails(long userId)
        {
            var mails = new List<Models.ZCMailItem>();
            var response = ZCXmlSerializer.Serialize(mails);
            return (true, response);
        }
    }
}