﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Connections;
using ZCure.API.Controllers;
using ZCure.API.Database;
using ZCure.API.Models;
using ZCure.API.Protocol;
using ZCure.API.Serializers;

namespace ZCure.API.Services
{
    public class FriendService : Service
    {
        private Repository<ZCFriendItem, long> _friendsRepository => _app.Database.Friends;
        private PresenceService _presence => _app.GetService<PresenceService>()!;

        public override void Initialize(ZCureApp app)
        {
            base.Initialize(app);
            app.GetService<UserService>()!.OnUserLoggedIn += NotifyNewFriendRequests;
        }

        public (EFriendAddResponse, ZCFriendItem?) CreateFriendRequest(User owner, User friend)
        {
            if (owner.Id == friend.Id)
                return (EFriendAddResponse.FriendYourself, null);

            var friendship = _friendsRepository.Where(new { OwnerId = owner.Id, FriendId = friend.Id }).FirstOrDefault();
            if (friendship != null)
            {
                EFriendAddResponse response;
                switch (friendship.State)
                {
                    case EFriendshipState.Pending:
                        response = EFriendAddResponse.AlreadyPending;
                        break;
                    case EFriendshipState.Accepted:
                        response = EFriendAddResponse.AlreadyFriend;
                        break;
                    case EFriendshipState.Denied:
                        response = EFriendAddResponse.AlreadyDenied;
                        break;
                    default:
                        response = EFriendAddResponse.UnknownError;
                        break;
                }

                return (response, null);
            }

            var ownFriendship = new ZCFriendItem()
            {
                OwnerId = owner.Id,
                FriendId = friend.Id,
                State = EFriendshipState.Pending,
                UniqueId = Guid.NewGuid(),
                AddedAt = DateTime.Now
            };

            var remoteFriendship = new ZCFriendItem()
            {
                OwnerId = friend.Id,
                FriendId = owner.Id,
                State = EFriendshipState.Pending,
                UniqueId = ownFriendship.UniqueId,
                AddedAt = DateTime.Now
            };

            _friendsRepository.Insert(ownFriendship);
            _friendsRepository.Insert(remoteFriendship);

            var friendNotificationData = $"{friend.Id}:0:{ZCXmlSerializer.Serialize(remoteFriendship)}";
            _presence.SendNotification(friend, Protocol.MessageType.FriendAdded, friendNotificationData);

            return (EFriendAddResponse.Success, ownFriendship);
        }

        public bool HandleFriendRequestResponse(User owner, User friend, bool accepted)
        {
            var myFriendship = _friendsRepository.Where(new { OwnerId = owner.Id, FriendId = friend.Id }).FirstOrDefault();
            var theirFriendship = _friendsRepository.Where(new { OwnerId = friend.Id, FriendId = owner.Id }).FirstOrDefault();

            if (myFriendship == null || theirFriendship == null || myFriendship.UniqueId != theirFriendship.UniqueId)
                return false;

            EFriendshipState state = accepted ? EFriendshipState.Accepted : EFriendshipState.Denied;
            myFriendship.State = state;
            theirFriendship.State = state;
            _friendsRepository.Update(myFriendship);
            _friendsRepository.Update(theirFriendship);

            if(accepted)
            {
                UpdateFriendNotification(owner, myFriendship);
                UpdateFriendNotification(friend, theirFriendship);
            }

            return true;
        }

        public void NotifyNewFriendRequests(User owner, ISession session)
        {
            // check for friend requests
            var friendRequests = _friendsRepository.Where(new { FriendId = owner.Id, State = EFriendshipState.Pending }).ToList();

            if (friendRequests.Count == 0)
                return;

            var data = $"{owner.Id}:0:{ZCXmlSerializer.Serialize(friendRequests)}";
            _presence.SendNotification(owner, MessageType.FriendAdded, data);
        }

        public void UpdateFriendNotification(User owner, ZCFriendItem friendship)
        {
            var data = $"{(friendship.State == EFriendshipState.Accepted ? 'T' : 'F')}:{ZCXmlSerializer.Serialize(new List<ZCFriendItem>() { friendship })}";
            _presence.SendNotification(owner, Protocol.MessageType.FriendUpdated, data);
        }
    }
}
