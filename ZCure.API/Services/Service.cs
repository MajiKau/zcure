﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZCure.API.Database;

namespace ZCure.API.Services
{
    public class Service : IService
    {
        protected ZCureApp _app;

        protected Repositories _database => _app.Database;

        protected ILogger _log;

        public virtual void Initialize(ZCureApp app) 
        {
            _app = app;
            _log = Log.ForContext(GetType());
        }

        public virtual void Shutdown() { }
    }
}
