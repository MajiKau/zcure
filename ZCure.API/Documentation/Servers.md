ZCure is designed to be network-agnostic, which means the same workflow works for different network protocols.
This is required to built an efficient server which allows both client-server and server-client(s) communication models.

## client-server (`main`)

The client-server (query-response) communication is implemented through TCP/HTTP connections and used to pull data from the ZCure server required by the client for common netorking features.

- authentication
- user data
    - profile
    - funds
    - friend list
    - mails
- inventory
    - owned items
    - loadouts
- store
    - available items
    - available offers
- clans
    - clan profile
    - user invitation/kicking
    - rank data
- small updates
    - primarely used to update config and translation files
    - possibly can be used to update data packages (like unreal packages) ?
- game server management
    - register/unregister/list available game servers
    - start/stop game servers on demand
- ...

## server-client (`presence`)

The server-client communication is implemented through UDP connections and used to notify client of recent events and data changes which are not subject to a definitive query.

It implements the functionality of the presence server.

- social
    - friend online status
- matchmaking
    - game server was found
- ... ?