﻿using ZCure.API.Servers;
using System.Net;
using ZCure.API;
using ZCure.API.Configuration;
using CommandLine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using System.Threading.Tasks.Sources;
using System.Runtime.Loader;

namespace ZCure.Server
{

    public class CliOptions
    {
        [Option('c', "config", Default = "zcure.yml", HelpText = "path to configuration file")]
        public string ConfigFile { get; set; } = "zcure.yml";
    }

    internal class Program
    {
        static void StartApp(CliOptions opts)
        {
            AppConfig config = new AppConfig();

            if(File.Exists(opts.ConfigFile))
            {
                var yamlSerializer = new DeserializerBuilder()
                    .WithNamingConvention(CamelCaseNamingConvention.Instance)
                    .Build();

                config = yamlSerializer.Deserialize<AppConfig>(File.ReadAllText(opts.ConfigFile));
            }
            config.MapEnvironmentVariables();

            ZCureApp app = new ZCureApp(config);
            app.Initialize();

            HttpServer httpServer = new HttpServer(app, IPAddress.Parse(app.Configuration.WebServer.Ip), app.Configuration.WebServer.Port, "HTTP", "INT");
            httpServer.Start();

            var presence = new TcpServer(app, IPAddress.Parse(app.Configuration.PresenceServer.Ip), app.Configuration.PresenceServer.Port);
            presence.Start();

            var endEvent = new ManualResetEventSlim();
            var startEnd = new ManualResetEventSlim();

            AssemblyLoadContext.Default.Unloading += (ctx) =>
            {
                startEnd.Set();
                endEvent.Wait();
            };

            startEnd.Wait();
            httpServer.Stop();
            presence.Stop();
            endEvent.Set();
        }

        static void Main(string[] args) =>
            CommandLine.Parser.Default
                .ParseArguments<CliOptions>(args)
                .WithParsed(StartApp);
    }
}