ZCure server for Blacklight: Retribution v3.02.

# build

- install .NET 8 SDK
- build using dotnet:
    - Debug: `$ dotnet build ZCure.sln -c Debug`
    - Release: `$ dotnet build ZCure.sln -c Release`

## debugging

### using VS Code (recommended)

- install [C# extension](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
- open project root in VS Code
- go to `Run and Debug`
- select `Debug Server` and execute

### using Visual Studio (>2019)

- open `ZCure.sln`  in VS
- select `ZCure.Server` as startup project
- compile and debug

